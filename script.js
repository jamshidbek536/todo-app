const input = document.querySelector(".input");
const addBtn = document.querySelector(".add-button");
const lists = document.querySelector(".lists");
const clear = document.querySelector(".clear");
let counter = document.querySelector(".counter");
let tasksList = [];

const Rendering = () => {
  lists.innerHTML = "";

  tasksList.forEach((item) => {
    const taskDone = item.isDone ? "text done" : "text";
    lists.innerHTML += `
    <li class="list">
      <span class="${taskDone}">${item.task}</span>
      <div class="icons">
        <img class="done" onclick="doneClick(\'${item.task}\')" src="./images/Pencil edit.svg" />
        <img class="delete" onclick="deleteClick(\'${item.task}\')" src="./images/cancel_24.svg" />
      </div>    
     </li>
    `;

    counter.innerHTML = `${tasksList.length}`;
  });
};

addBtn.addEventListener("click", () => {
  let value = input.value;
  if (value.trim()) {
    tasksList.unshift({ task: value, isDone: false });
    input.value = "";
  } else {
    alert("Vazifalarni kiriting");
  }
  Rendering();
});

function deleteClick(task) {
  const currentIndex = tasksList.findIndex((v) => v.task === task);
  tasksList.splice(currentIndex, 1);
  Rendering();
}

function doneClick(task) {
  const currentIndex = tasksList.findIndex((v) => v.task === task);
  tasksList[currentIndex].isDone = true;
  Rendering();
}

clear.addEventListener("click", () => {
  tasksList = [];
  counter.innerHTML = 0;
  Rendering();
});
